var SPREADSHEET_CODE = '1t5qrB9BHtNveaVn9--_JKQCOO4PLZiCyzzz14gALEoY'
function getRequestUrl(spreadsheetCode, pageNumber) {
	return 'https://spreadsheets.google.com/feeds/cells/'
		+ spreadsheetCode
		+ '/'
		+ pageNumber
		+ '/public/full?alt=json'
}

var requestUrl = getRequestUrl(SPREADSHEET_CODE, 1)

var mapFromColumnNumberToKey = {
	1: 'lat',
	2: 'lon',
	3: 'obec',
	4: 'adresa',
	5: 'skupina',
}

function getKeyForColNum(colNum) {
	return mapFromColumnNumberToKey[colNum]
}

function getLeafletPointsFromJson (jsonResponse) {
	var tableData = jsonResponse.feed.entry
	
	var leafletPoints = []
	
	tableData.forEach( function(cellObject) {
		var cellData = cellObject.gs$cell
		var rowNum = cellData.row
		var colNum = cellData.col
		var value  = cellData.inputValue
		
		var currentPoint = leafletPoints[rowNum]
		
		// Vytvoří objekt pro pro daný řádek, pokud ještě neexistuje
		if (!currentPoint) {
			leafletPoints[rowNum] = { id: rowNum - 2 } // -2 je kvůli shiftům na konci
			currentPoint = leafletPoints[rowNum]
		}
		
		currentPoint[getKeyForColNum(colNum)] = value
	})
	
	leafletPoints.shift() // Posune indexy, aby začínaly od 0 - jsou od 1, protože jsou podle čísla řádku
	leafletPoints.shift() // Odstraní první řádek, bo tam je hlavička tabulky
	
	return leafletPoints
}


$.getJSON(requestUrl, function(jsonResponse) {
	var points = getLeafletPointsFromJson(jsonResponse)
	
	console.log(points)
})
